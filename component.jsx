// Comment Class
class Comment extends React.Component {
    render() {
        return (<div className="comment">
                <p className="comment-header">{this.props.author} </p>
                <p className="comment-body">
                    {this.props.body}
                </p>
                <div className="comment-footer">
                    <a href="#" className="comment-footer-delete">
                        Delete Comments
                    </a>
                </div>
            </div>);
    }
}

// Comment Form Class

class CommentForm extends React.Component{
    render(){
        return(
        <form className="comment-form" onSubmit={this._handleSubmit.bind(this)}>
            <label>Join the Discussion</label>
                <div className="comment-form-fields">
                    <input placeholder="Name:" ref={(input)=>this._author=input}/>
                    <textarea placeholder="Comment:" ref={(textarea)=>this._body=textarea}></textarea>
                </div>
                <div className="comment-form-actions">
                <button type="submit">
                    Post comment</button>
                </div>
            </form>
        )
    };
    
    _handleSubmit(event)
    {
        event.preventDefault();
        let author = this._author;
        let body = this._body;
        console.log("boo")
        this.props.addComment(author.value, body.value);
    }
}

// CommentBox Class
class CommentBox extends React.Component {
    
    constructor()
    {
        super();
        this.state={
            showComments:false,
            comments:[
    {id:1,author:'Morgan McCircuit', body: 'Great picture!'},
                  {id:2,author:'Bending Bender', body: 'Excellent Stuff!!'}
   
]
        }
    }

_getCommentsTitle(commentCount){
    if (commentCount===0)
        return 'No comments yet';
    else if (commentCount ===1)
        return '1 comment';
    else
        return commentCount + ' comments' ;
}
    
    
    
    _getComments(){

              const commentList = this.state.comments;


        return commentList.map((comment) =>{
            return (<Comment author={comment.author}     key={comment.id}/>);
        } );

    }
    
    _handleClick(){
        this.setState(
            {showComments:!this.state.showComments}
        )
    }

    render() {

        const comments = this._getComments();
        
        let commentNodes;
        let buttonText = 'Show comments';
        
        if (this.state.showComments){
            buttonText = "Hide comments";
            commentNodes = <div className="comment-list">
        {comments}
        </div>
        }

        return ( 
        
            <div className="comment-box">
                    <CommentForm addComment={this._addComment.bind(this)}/>
                          <button onClick={this._handleClick.bind(this)}>{buttonText}</button>
                <h3>Comments</h3>
                <h4 className="comment-count"> { this._getCommentsTitle(comments.length)} </h4>
                {commentNodes}
            </div>
        );
    }
    
    
    _addComment(author,body){
        const comment = {
                            id:this.state.comments.length + 1,
                            author,
                            body
                        }
        console.log("Adding a comment" + author);
        this.setState({comments:this.state.comments.concat([comment])});
    }
    
}


ReactDOM.render(
    <CommentBox />, document.getElementById('mount-app')
);

